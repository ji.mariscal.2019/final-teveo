from django.test import TestCase, Client
from .models import Camara

class HomePageTestCase(TestCase):
    def test_home_page(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Pages/home.html')

class CamarasPageTestCase(TestCase):
    def test_camaras_page(self):
        response = self.client.get('/camaras/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Pages/cams_page.html')

class ComentarioTestCase(TestCase):
    def test_comentario(self):
        camara = Camara.objects.create(Id_cam='1', Name='Test Camera', Coord='0,0', Src='https://example.com/test.jpg')
        response = self.client.get(f'/comentario/?id={camara.id}')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Pages/comentario.html')

class AyudaTestCase(TestCase):
    def test_ayuda(self):
        response = self.client.get('/ayuda/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Pages/help.html')

class ConfigTestCase(TestCase):
    def test_config(self):
        # Crear una sesión de prueba
        client = Client()
        session = client.session
        session.save()  # Guardar la sesión para obtener una session_key válida
        # Realizar la solicitud GET a /configuracion/
        response = client.get('/configuracion/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Pages/settings.html')

class CamarasJsonTestCase(TestCase):
    def test_camaras_json(self):
        camara = Camara.objects.create(Id_cam='1', Name='Test Camera', Coord='0,0', Src='https://example.com/test.jpg')
        response = self.client.get(f'/camara_{camara.id}_json/')
        self.assertEqual(response.status_code, 200)
        self.assertJSONEqual(response.content, [{'id': camara.Id_cam, 'nombre': camara.Name, 'ubicacion': camara.Coord, 'fuente': camara.Src}])

class FaviconTestCase(TestCase):
    def test_favicon(self):
        response = self.client.get('/favicon.ico')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(response['Content-Type'], 'image/x-icon')

class CamaraImgTestCase(TestCase):
    def test_camara_img(self):
        camara = Camara.objects.create(Id_cam='1', Name='Test Camera', Coord='0,0', Src='https://example.com/test.jpg')
        response = self.client.get(f'/{camara.id}-dyn/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Pages/cam_dyn.html')

class CamaraProfileTestCase(TestCase):
    def test_camara_profile(self):
        camara = Camara.objects.create(Id_cam='1', Name='Test Camera', Coord='0,0', Src='https://example.com/test.jpg')
        response = self.client.get(f'/{camara.id}/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'Pages/cam_profile.html')