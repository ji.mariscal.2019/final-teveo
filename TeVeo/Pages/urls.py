from django.urls import path
from . import views

urlpatterns = [
    path('camara_<int:id>_json/', views.camaras_json, name='camaras_json'),
    path('favicon.ico', views.favicon, name='favicon'),
    path('camaras/', views.camaras_page, name='camaras_page'),
    path('ayuda/', views.ayuda, name='ayuda'),
    path('configuracion/', views.config, name='config'),
    path('comentario/', views.comentario, name='comentario'),
    path('<int:id>-dyn/', views.camara_img, name='camara_img'),
    path("image_<int:id>/", views.descargar_y_devolver_imagen),
    path('<int:id>/', views.camara_profile, name='camara_profile'),
    path('', views.home_page, name='home_page'),
]