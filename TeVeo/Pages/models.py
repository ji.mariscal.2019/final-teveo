# Create your models here.
from django.db import models

#Cuando creas/modificas un modelo debes hacer makemigrations y migrate

class Usersetup(models.Model):
    Username = models.CharField(max_length=100, default='Anónimo')
    Session_id = models.CharField(max_length=200, default='550e8400-e29b-41d4-a716-446655440000')
    Font_size = models.CharField(max_length=10, default='medium')  # small, medium, large
    Font_type = models.CharField(max_length=100, default='Arial')  # Arial, Times New Roman, etc.
    def __str__(self):
        return self.Username

class Camara(models.Model):
    Id_cam = models.CharField(max_length=128)
    Name = models.CharField(max_length=200)
    Coord = models.CharField(max_length=200)
    Src = models.URLField(max_length=200)
    def __str__(self):
        return self.Name


#Cada Cámara tiene  N comentarios
class Comentario(models.Model):
    Camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    Usuario = models.CharField(max_length=128)
    Cuerpo = models.TextField()
    Fecha = models.DateTimeField()
    def __str__(self):
        return self.Cuerpo

class Like(models.Model):
    Camara = models.ForeignKey(Camara, on_delete=models.CASCADE)
    Usersetup = models.ForeignKey(Usersetup, on_delete=models.CASCADE)
    def __str__(self):
        return f"Like del usuario {self.Usersetup.Username} a la cámara {self.Camara.Name}"