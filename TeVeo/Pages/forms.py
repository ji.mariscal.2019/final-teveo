from django import forms
from .models import Comentario, Usersetup

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['Cuerpo']
        widgets = {
            'Cuerpo': forms.Textarea(attrs={
                'class': 'form-control',
                'placeholder': 'Escribe tu comentario aquí...',
                'rows': 5,  # Ajusta el número de filas según tus necesidades
                'style': 'resize:none;',  # Evita el redimensionamiento del textarea
            }),
        }

class UsersetupForm(forms.ModelForm):
    class Meta:
        model = Usersetup
        fields = ['Username', 'Font_size', 'Font_type']
        widgets = {
            'Username': forms.TextInput(attrs={'placeholder': 'Nombre de comentador'}),
            'Font_size': forms.Select(choices=[('small', 'Pequeño'), ('medium', 'Mediano'), ('large', 'Grande')]),
            'Font_type': forms.Select(choices=[('Arial', 'Arial'), ('Times New Roman', 'Times New Roman'), ('American Typewriter', 'American Typewriter')])
        }

