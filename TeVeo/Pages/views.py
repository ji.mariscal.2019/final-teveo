from django.shortcuts import render, redirect
from django.http import HttpResponse, request, JsonResponse
from .models import Camara, Comentario, Usersetup, Like
from django.template import loader
from django.views.decorators.csrf import csrf_exempt
from django.utils import timezone
from .forms import ComentarioForm, UsersetupForm
import base64
import requests
import random
import string
from xml.etree import ElementTree
from django.db.models import Count
from django.core.paginator import Paginator


@csrf_exempt
def home_page(request):
    if not request.session.session_key:
        request.session.create()
    user_anonimo = Usersetup.objects.filter(Username='Anónimo').first()
    if not user_anonimo:
        user_anonimo = Usersetup.objects.create(
            Username='Anónimo',
            Session_id=''.join(random.choice(string.ascii_lowercase + string.digits) for _ in range(32)),
            Font_size='medium',
            Font_type='Arial'
        )
    session_key = request.session.session_key
    usersetup = Usersetup.objects.filter(Session_id=session_key).first()
    if not usersetup:
        usersetup = Usersetup(Username='Anónimo')
    camaras = Camara.objects.all()
    comentarios = Comentario.objects.all().order_by('-Fecha')
    paginator = Paginator(comentarios, 10)  # 10 comentarios por página
    page_number = request.GET.get('page')
    comentarios = paginator.get_page(page_number)
    num_camaras = Camara.objects.count()
    num_comentarios = Comentario.objects.count()
    template = loader.get_template('Pages/home.html')
    context = {"camaras": camaras, "comentarios": comentarios, 'num_camaras': num_camaras, 'num_comentarios': num_comentarios, 'usersetup': usersetup}
    return HttpResponse(template.render(context, request))

def camaras_page(request):
    session_key = request.session.session_key
    usersetup = Usersetup.objects.filter(Session_id=session_key).first()
    if not usersetup:
        usersetup = Usersetup(Username='Anónimo')
    if 'source' in request.GET:
        source_url = request.GET['source']
        response = requests.get(source_url)
        tree = ElementTree.fromstring(response.content)

        if tree.tag == 'camaras':
            for cam_element in tree.findall('camara'):
                cam_id = cam_element.find('id').text
                cam_id = "LIS1-" + cam_id
                cam_src = cam_element.find('src').text
                cam_name = cam_element.find('lugar').text
                cam_coord = cam_element.find('coordenadas').text
                if not Camara.objects.filter(Id_cam=cam_id).exists():
                    Camara.objects.create(
                        Id_cam=cam_id,
                        Name=cam_name,
                        Coord=cam_coord,
                        Src=cam_src
                    )
        if tree.tag.endswith('d2LogicalModel'):
            for cam_record in tree.findall('.//_0:cctvCameraMetadataRecord',
                                           namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}):
                cam_id = cam_record.get('id')
                cam_id = "LIS3-" + cam_id
                cam_name = cam_record.find('_0:cctvCameraIdentification',
                                           namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                cam_src = cam_record.find('.//_0:urlLinkAddress',
                                          namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                cam_latitude = cam_record.find('.//_0:latitude',
                                               namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                cam_longitude = cam_record.find('.//_0:longitude',
                                                namespaces={'_0': 'http://datex2.eu/schema/2/2_0'}).text
                cam_coord = "{},{}".format(cam_latitude, cam_longitude)

                if not Camara.objects.filter(Id_cam=cam_id).exists():
                    Camara.objects.create(
                        Id_cam=cam_id,
                        Name=cam_name,
                        Coord=cam_coord,
                        Src=cam_src
                    )
        elif tree.tag == 'list':
            for cam_element in tree.findall('cam'):
                cam_id = cam_element.get('id')
                cam_id = "LIS2-" + cam_id
                cam_src = cam_element.find('url').text
                cam_name = cam_element.find('info').text
                cam_coord = "{},{}".format(cam_element.find('place/latitude').text,
                                           cam_element.find('place/longitude').text)
                if not Camara.objects.filter(Id_cam=cam_id).exists():
                    Camara.objects.create(
                        Id_cam=cam_id,
                        Name=cam_name,
                        Coord=cam_coord,
                        Src=cam_src
                    )
        elif tree.tag == '{http://earth.google.com/kml/2.2}kml':
            for placemark in tree.findall('.//{http://earth.google.com/kml/2.2}Placemark'):
                cam_id = placemark.findtext(
                    '{http://earth.google.com/kml/2.2}ExtendedData/{http://earth.google.com/kml/2.2}Data[@name="Numero"]/{http://earth.google.com/kml/2.2}Value')
                cam_id = "LIS4-" + cam_id
                cam_name = placemark.findtext(
                    '{http://earth.google.com/kml/2.2}ExtendedData/{http://earth.google.com/kml/2.2}Data[@name="Nombre"]/{http://earth.google.com/kml/2.2}Value')
                cam_coord = placemark.findtext(
                    '{http://earth.google.com/kml/2.2}Point/{http://earth.google.com/kml/2.2}coordinates')
                cam_src = placemark.findtext('{http://earth.google.com/kml/2.2}description')
                cam_src = cam_src.split('<img src=')[1].split(' ')[0]

                if not Camara.objects.filter(Id_cam=cam_id).exists():
                    Camara.objects.create(
                        Id_cam=cam_id,
                        Name=cam_name,
                        Coord=cam_coord,
                        Src=cam_src
                    )

        return redirect('/camaras')

    # Código para mostrar la página con las cámaras
    camaras = Camara.objects.annotate(num_comentarios=Count('comentario')).order_by('-num_comentarios')
    paginator = Paginator(camaras, 10)  # 10 cámaras por página
    page_number = request.GET.get('page')
    camaras = paginator.get_page(page_number)
    camara_aleatoria = random.choice(camaras) if camaras else None
    num_camaras = Camara.objects.count()
    num_comentarios = Comentario.objects.count()
    data_sources = [
        "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml",
        "https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml",
        "https://infocar.dgt.es/datex2/dgt/CCTVSiteTablePublication/all/content.xml",
        "http://datos.madrid.es/egob/catalogo/202088-0-trafico-camaras.kml"
    ]
    context = {
        "camaras": camaras,
        "camara_aleatoria": camara_aleatoria,
        "data_sources": data_sources,
        'num_camaras': num_camaras,
        'num_comentarios': num_comentarios,
        'usersetup': usersetup
    }
    return render(request, 'Pages/cams_page.html', context)

def descargar_y_devolver_imagen(request, id):
    try:
        # URL de la imagen a descargar
        cam = Camara.objects.get(id=id)
        url_imagen = cam.Src
        # Realizar la solicitud HTTP para obtener la imagen
        headers = {
            'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:100.0) Gecko/20100101 Firefox/100.0'
        }
        # Realizar la solicitud HTTP para obtener la imagen
        response = requests.get(url_imagen, headers=headers)
        response.raise_for_status()  # Lanza una excepción si la solicitud no fue exitosa

        # Extraer el contenido de la respuesta
        imagen_bytes = response.content
        # Convertir la imagen a formato Base64
        imagen_base64 = base64.b64encode(response.content).decode('utf-8')

        devuelvo='<img src="data:image/jpeg;base64,##content##">'
        devuelvo=devuelvo.replace('##content##',imagen_base64)
        # Devolver la imagen como una HttpResponse con el tipo de contenido adecuado
        return HttpResponse(devuelvo, content_type="image/jpeg")
    except Exception as e:
        return HttpResponse(str(e), status=500)


def camara_img(request, id):
    session_key = request.session.session_key
    usersetup = Usersetup.objects.filter(Session_id=session_key).first()
    if not usersetup:
        usersetup = Usersetup(Username='Anónimo')
    if request.method =="GET":
        cam = Camara.objects.get(id=id)
        com = Comentario.objects.filter(Camara=cam)
        num_camaras = Camara.objects.count()
        num_comentarios = Comentario.objects.count()
        num_likes = Like.objects.filter(Camara=cam).count()
        if request.method == 'POST':
            if usersetup.Username == "Anónimo":
                return redirect('camara_profile', id=id)  # Otra acción que desees tomar si el usuario es anónimo
            # Manejar la solicitud POST para registrar un "like" si no existe ya una combinación de cámara y usuario
            if not Like.objects.filter(Camara=cam, Usersetup=usersetup).exists() and usersetup.Username != "Anónimo":
                like = Like(Camara=cam, Usersetup=usersetup)
                like.save()
                # Redirigir de nuevo a la página de perfil de la cámara después de registrar el "like"
                return redirect('camara_profile', id=id)
        template = loader.get_template('Pages/cam_dyn.html')
        context = {"camara": cam, "comentarios": com, 'num_camaras': num_camaras, 'num_comentarios': num_comentarios, "num_likes": num_likes, 'usersetup': usersetup}
        return HttpResponse(template.render(context, request))


def camara_profile(request, id):
    session_key = request.session.session_key
    usersetup = Usersetup.objects.filter(Session_id=session_key).first()
    if not usersetup:
        usersetup = Usersetup(Username='Anónimo')
    cam = Camara.objects.get(id=id)
    com = Comentario.objects.filter(Camara=cam)

    # Contar cuántos usuarios están asociados a esta cámara
    num_likes = Like.objects.filter(Camara=cam).count()

    num_camaras = Camara.objects.count()
    num_comentarios = Comentario.objects.count()

    if request.method == 'POST':
        if usersetup.Username == "Anónimo":
            return redirect('camara_profile', id=id)  # Otra acción que desees tomar si el usuario es anónimo
        # Manejar la solicitud POST para registrar un "like" si no existe ya una combinación de cámara y usuario
        if not Like.objects.filter(Camara=cam, Usersetup=usersetup).exists() and usersetup.Username != "Anónimo":
            like = Like(Camara=cam, Usersetup=usersetup)
            like.save()
            # Redirigir de nuevo a la página de perfil de la cámara después de registrar el "like"
            return redirect('camara_profile', id=id)

    template = loader.get_template('Pages/cam_profile.html')
    context = {
        "camara": cam,
        "comentarios": com,
        "num_camaras": num_camaras,
        "num_comentarios": num_comentarios,
        "num_likes": num_likes,
        'usersetup': usersetup # Agregamos el número de usuarios asociados a esta cámara
    }

    return HttpResponse(template.render(context, request))
def comentario(request):  #/comentario/?id=1
    session_key = request.session.session_key
    usersetup = Usersetup.objects.filter(Session_id=session_key).first()
    if not usersetup:
        usersetup = Usersetup(Username='Anónimo')
    camera_id = request.GET.get('id')
    if camera_id:
        camara = Camara.objects.get(id=camera_id)
        num_camaras = Camara.objects.count()
        num_comentarios = Comentario.objects.count()
        if request.method == 'POST':
            form = ComentarioForm(request.POST)
            if form.is_valid():
                comentario = form.save(commit=False)
                comentario.Usuario = usersetup.Username
                comentario.Camara = camara
                comentario.Fecha = timezone.now()
                comentario.save()
                return redirect(f'/{camera_id}')  # Redirige a la página de la cámara
        else:
            form = ComentarioForm()
        return render(request, 'Pages/comentario.html', {'form': form, 'num_camaras': num_camaras, 'num_comentarios': num_comentarios, 'usersetup': usersetup})
    else:
        return HttpResponse("No se proporcionó un identificador de cámara.", status=400)

def ayuda(request):
    session_key = request.session.session_key
    usersetup = Usersetup.objects.filter(Session_id=session_key).first()
    if not usersetup:
        usersetup = Usersetup(Username='Anónimo')
    num_camaras = Camara.objects.count()
    num_comentarios = Comentario.objects.count()
    template = loader.get_template('Pages/help.html')
    context = {'num_camaras': num_camaras, 'num_comentarios': num_comentarios, 'usersetup': usersetup}
    return HttpResponse(template.render(context, request))

def config(request):
    # Obtener la configuración del usuario actual basada en la sesión
    session_key = request.session.session_key
    settings = Usersetup.objects.filter(Session_id=session_key).first()
    if not settings:
        # Si no existe configuración para esta sesión, usa el perfíl Anónimo
        settings = Usersetup(Username='Anónimo')
    num_camaras = Camara.objects.count()
    num_comentarios = Comentario.objects.count()

    if request.method == 'POST':
        if 'logout' in request.POST:
            if settings.Username == "Anónimo":
                return redirect('home_page')
            else:
                settings.delete()
                return redirect('home_page')

        form = UsersetupForm(request.POST, instance=settings)
        if form.is_valid():
            session_key = request.session.session_key
            usersetup = form.save(commit=False)
            usersetup.Session_id = session_key
            usersetup.save()

            # Verificar si se ha solicitado un enlace de autorización
            if 'authorize_link' in request.POST:
                # Generar un identificador único para la sesión
                session_key = request.session.session_key
                # Generar la URL de autorización
                authorize_url = f"{request.build_absolute_uri()}?{session_key}"
                return render(request, 'Pages/settings.html', {
                    'form': form,
                    'num_camaras': num_camaras,
                    'num_comentarios': num_comentarios,
                    'usersetup': settings,
                    'authorize_url': authorize_url
                })
            else:
                return redirect('home_page')  # Redirigir a la página principal tras guardar
    else:
        # Inicializar el formulario con la instancia de Usersetup que puede tener valores por defecto
        form = UsersetupForm(instance=settings)

        # Verificar si se ha proporcionado una URL de sesión
        session_url = request.GET.get('session_url')
        if session_url:
            try:
                # Extraer la ID de sesión de la URL
                session_id = session_url.split('?')[-1]
                # Buscar la configuración con la ID de sesión
                authorized_settings = Usersetup.objects.get(Session_id=session_id)
                # Copiar la configuración a la sesión actual
                settings.Username = authorized_settings.Username
                settings.Font_size = authorized_settings.Font_size
                settings.Font_type = authorized_settings.Font_type
                settings.save()
                return redirect(f'/configuracion/')

            except (Usersetup.DoesNotExist, ValueError):
                error_message = "No existe esa ID de la sesión"
                return render(request, 'Pages/settings.html', {
                    'form': form,
                    'num_camaras': num_camaras,
                    'num_comentarios': num_comentarios,
                    'usersetup': settings,
                    'error_message': error_message
                })

    return render(request, 'Pages/settings.html', {
        'form': form,
        'num_camaras': num_camaras,
        'num_comentarios': num_comentarios,
        'usersetup': settings
    })

def favicon(request):
    favicon_path = 'Static/Media/cam_favico.ico'
    with open(favicon_path, 'rb') as f:
        favicon_content = f.read()
    return HttpResponse(favicon_content, content_type="image/x-icon")


def camaras_json(request, id):
    # Recuperar todas las cámaras
    camara = Camara.objects.get(id=id)
    data = []
    # Agregar la información de cada cámara a la lista
    data.append({
        'id': camara.Id_cam,
        'nombre': camara.Name,
        'ubicacion': camara.Coord,
        'fuente': camara.Src
    })
    return JsonResponse(data, safe=False)



