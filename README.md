# ENTREGA CONVOCATORIA MAYO

# ENTREGA DE PRÁCTICA

## Datos

- **Nombre:** Jose Ignacio Mariscal Cristóbal.
- **Titulación:** Doble Grado en Sistemas de Telecomunicaciones y Administración y Dirección de Empresas.
- **Cuenta en laboratorios:** nachete.
- **Cuenta URJC:** ji.mariscal.2019@alumnos.urjc.es.
- **Video básico (url):** [URL del Video Básico](https://youtu.be/28SNE4Q9aLM)
- **Video parte opcional (url):** [URL del Video Parte Opcional](https://youtu.be/HwrRJ6fklbo)
- **Despliegue (url):** [URL del Despliegue](http://jimariscal2019.pythonanywhere.com/)
- **Contraseñas:** No se necesita ninguna.
- **Cuenta Admin Site:** admin/admin.

## Resumen parte obligatoria

- **GET /**
  - La página principal ofrece un listado de comentarios organizados de más nuevos a más viejos, con la información de cada comentario, incluyendo el identificador de la cámara, la fecha del comentario, el texto del comentario y la imagen de la cámara en el momento en que se puso el mensaje.
- **GET /camaras/**
  - La página de cámaras presenta un listado de fuentes de datos disponibles, con un botón junto a cada una para descargar la fuente de datos y almacenar la información correspondiente en la base de datos. También muestra un listado de todas las cámaras, ordenadas por número de comentarios, con la imagen de una cámara aleatoria en la parte superior. Para cada cámara, se muestra su identificador, nombre, número de comentarios y enlaces a la página de la cámara y a la página dinámica de la cámara.
- **GET /{id}/**
  - La página de cada cámara muestra información detallada de la cámara, incluyendo enlaces a la página dinámica de la cámara, nombre y localización, imagen actual de la cámara, enlace para escribir un comentario y un listado de todos los comentarios para esa cámara.
- **GET /comentario/**
  - La página para poner un comentario acepta un parámetro que indica el identificador de la cámara y muestra la información de la cámara, la imagen actual, la fecha y hora, y un formulario para poner el texto del comentario.
- **GET /{id}-dyn/**
  - La página dinámica de cada cámara es similar a la página de la cámara, pero utiliza HTMX para solicitar periódicamente la imagen y actualizar los comentarios cada 30 segundos, además de cargar el formulario para poner un comentario al pulsar el enlace correspondiente.
- **GET /configuracion/**
  - La página de configuración permite configurar el sitio para ese navegador, incluyendo un formulario para elegir el nombre con el que se pondrán los comentarios y los elementos de apariencia personalizables del sitio.
- **GET /ayuda/**
  - Finalmente, la página de ayuda proporciona información sobre la autoría de la práctica, su funcionamiento y una breve documentación.

## Lista partes opcionales

- Inclusión de las dos fuentes opcionales propuestas.
- Inclusión de un Favico.ico.
- Inclusión de un botón para "Cerrar Sesión".
- Paginación.
- Función "Me gusta".
- Aspecto atractivo de la aplicación.

